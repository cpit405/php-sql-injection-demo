# SQL injection demo app
This is a demo for an app that is vulnerable to SQL injection due to executing SQL statements directly via string concatenations without using prepared statements. 

This is a todo app written in PHP and uses PostgreSQL as a database to store todo items. It demonostrates the risk of not using prepared statements to protect against SQL injections when building CRUD apps (Create, Retrieve, Update, and Delete). This demo uses both the database-independent extension, _PDO_, and vendor specific database extension for PostgreSQL, _pgsql_. The code for each database extension is in their respected repository branch.

## Usage

### Setting up the database

You will need to install PostgreSQL and connect to the databse server using either the command line client `psql -U username -W` or a GUI client like [pgAdmin](https://www.pgadmin.org/).
First, we will create a file called `database.ini` that contains the database connection parameters and access credentials. Example:

```plaintext
host=localhost
port=5432
database=tasks
user=YOUR_POSTGRESDB_USER_NAME
password=YOUR_PASSWORD
```

Next, we will create a table called *tasks* with the following definition:

```
+------------+--------------------------+------+-----+---------+
| Field      | Type                     | Null | Key | Default |
+------------+--------------------------+------+-----+---------+
| id         | SERIAL                   | NO   | PRI | NULL    |
| task       | CHARACTER varying(255)   | NO   |     | NULL    |
| date_added | TIME with time zone      | NO   |     | NULL    |
| done       | BOOLEAN DEFAULT          | NO   |     | FALSE   |
+------------+--------------------------+------+-----+---------+
```

Below is the SQL code for creating the database and table as well as the user and role for our php app that will access the database. Please make sure that the password you choose when creating a user is the same password defined in your `database.ini`.

```sql
/* create a database */
CREATE DATABASE tododb;
/* Connect to the database */
\c tododb;
/* Create table */
CREATE TABLE IF NOT EXISTS tasks (
    id SERIAL PRIMARY KEY,
    task CHARACTER varying(255) NOT NULL,
    date_added TIME with time zone NOT NULL,
    done BOOLEAN DEFAULT false NOT NULL
);
```


### Install and enable the PostgreSQL PDO database driver for PHP

You will need to install and enable the PHP PDO PostgreSQL driver in the configuration file (`php.ini` file). In the `php.ini` file, you can find the line that contains `extension=php_pdo_pgsql` and uncomment it (remove the semicolon).


### Running the PHP app
Once the database has been installed and the table has been created, simply run the app by serving it from a web server. You may use any web server to serve the app (e.g., Apache HTTP Server, nginx) or PHP's built-in web server:

`php -S localhost:8080 -t ./`


## SQL Injection Attack scenario
This app doesn't implement any defenses against SQL injection attacks, so an attacker could construct an attack by entering data (SQL statements) in the form fields of the application, which is then passed to the database for processing.

This app does not properly validate or sanitize the form input data, allowing an attacker to inject SQL statements via form fields and be able to delete, copy, or modify the contents of the database.

For example, when a new task (e.g., "Buy milk") is added to the todo list, the app executes the following PHP statement:

```php
$sql_query = "INSERT INTO tasks (task, date_added, done) VALUES('".$new_task."','".$date."',DEFAULT)";

```
which transaltes into something like:

```sql
INSERT INTO tasks (task, date_added, done) VALUES('Buy milk', '2022-06-01 10:30", DEFAULT)
```

Instead of inserting a normal todo list item like "Buy milk", the malicious user may insert SQL statements or special synatx like:

- Input 1: `'--`
  - This translates into a comment causing the SQL insert statement to fail:
  
  ```sql
  INSERT INTO tasks (task, date_added, done) VALUES(''--', '2022-06-02 10:30", DEFAULT)
   ```
- Input 2: ` `
## Protection against SQL injection attacks

PDO Prepared statements/parameterized queries with bound parameters are generally the 1st protection that may be sufficient to prevent SQL injection attacks. There are additional security measures that need to be applied for your app to protect against SQL injection attacks.
