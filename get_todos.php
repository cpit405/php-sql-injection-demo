<?php 
function get_all_todos()
{
    $sql_query = "SELECT id, task, date_added, done FROM tasks WHERE done = false;";
    $statement = $GLOBALS['conn']->query($sql_query);
    if ($statement && $statement->columnCount()> 0) {
        echo '<ul id="my-list">';
        while($row = $statement->fetch()) {
            echo "<li>".'<input type="checkbox" name="checkBoxList[]" value="'.$row["id"].'"><span>'.$row["task"]."</span></li>";
        }
        echo '</ul>';
    } else {
        echo '<h2>Your ToDo list is empty!</h2>';
    }
}
